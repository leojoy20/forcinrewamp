declare module "*.json" {
  const jsons: any;
  export = jsons;
}

declare module "*.jpg" {
  const jpg: string;
  export = jpg;
}
declare module "*.png" {
  const jpg: string;
  export = jpg;
}

declare module "redux-loading-middleware" {
  const jpg: any;
  export = jpg;
}
declare module "react-viewer" {
  const jpg: any;
  export = jpg;
}


declare module "react-id-swiper" {
  const Swiper: any;
  export default Swiper;
}

declare module "react-select/*" {
  const Swiper: any;
  export default Swiper;
}

declare module "react-scroll-up-button*" {
  const CircleArrow: any;
  const VerticleButton: any;
  export { CircleArrow, VerticleButton };
}
