// import { Field } from "../node_modules/@types/redux-form";

import { Reducer } from "redux";
export interface FormStateMap {
    [formName: string]: FormState;
}
export interface FormState {
    registeredFields: any;
    fields?: {[name: string]: any};
    values?: { [fieldName: string]: any };
    active?: string;
    anyTouched?: boolean;
    submitting?: boolean;
    submitErrors?: { [fieldName: string]: string };
    submitFailed?: boolean;
    onSubmit:any
}
interface reducer  extends Reducer<FormStateMap>
{

}

declare module 'redux-form'{
    export const reduxForm:any
    export const Field:any
    export const reducer: reducer

}
