export const ApiUrls = {
  registerDriver: "delivery/driver/register/",
  login: "auth/token/",
  registrations: "delivery/driver/registrations/",
  approveReject: "delivery/driver/registrations/",
  updateProfile: "delivery/driver/profile/"
}
