import { Component } from "react";
import * as React from "react";
import classnames from 'classnames';
import { CustomProps } from "./props";
// import { setImageViewer } from '@forcin/actions/ui/utils';

export class RenderFileInput extends Component<CustomProps> {
  constructor(props: any) {
    super(props)
    this.onChange = this.onChange.bind(this)
  }

  onChange(e: any) {
    const { input: { onChange } } = this.props

    onChange(e.target.files[0])
  }
  showViewer(){
    // store.dispatch(
    //   setImageViewer(this.props.input.value)
    // )
  }
  render() {
    const { label, containerClass, input ,meta: { touched, error, warning }} = this.props  //whatever props you send to the component from redux-form Field
    let hasLabel = (typeof label != "undefined" || label != null);
    let text = "Upload File"
    let hasUpload = false
    if (typeof input.value == "string" && input.value.length>0 ) {
      text = input.value.substring(input.value.lastIndexOf("/") + 1, input.value.length)
      hasUpload = true
    }
    else if(input.value.name){
      text = input.value.name
    }
    return (
      <div className={classnames({ [containerClass]: true, "row": true })}>
        <label className="col-3">{label}</label>
        <div className={classnames({ "col-9": hasLabel, "col-12": !hasLabel })} >
          {/* {
            input.value
          } */}


          <div className="upload-btn-wrapper">
            <button className="btn"> {text}  </button>

            <input type="file" name="myfile" accept="image/*" onChange={this.onChange} />
          </div>
          {hasUpload && <a onClick={this.showViewer.bind(this)} className="bt btn-sm btn-rounded-trans"><i className="fa fa-eye"></i></a>}


        </div>
      {touched && ((error && <span className="help-block  text-danger"> {error} </span>) || (warning && <span>{warning}</span>))}

      </div>

    )
  }
}
