export interface CustomProps {
  input: any,
  label: any,
  required: Boolean,
  meta: any,
  containerClass:string
  dispatch:any
}
