import { MetaProps } from '@forcin/library/redux-fom-utils/props';

export interface CustomProps {
  input?: any,
  label?: any,
  required?: Boolean,
  meta?: MetaProps,
  containerClass?:string
  data?:any,
  textField?:string,
  filter?:any,
  searchTerm?:any,

  defaultOpen?:any,
  itemComponent?:any,
  valueField?:any,
  onSearch?:any,
  placeholder?:any
}
