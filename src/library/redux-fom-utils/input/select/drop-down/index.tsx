import { DropdownList } from 'react-widgets'
import { Component } from "react";
import * as React from "react";
import { CustomProps } from "./props";
import classnames = require('classnames');

export class RenderDropDownInput extends Component<CustomProps> {
  constructor(props: any) {
    super(props)
    this.onChange = this.onChange.bind(this)

  }

  onChange(e: any) {

    const { input: { onChange } } = this.props

    if (this.props.valueField)
      onChange(e[this.props.valueField].toString())
    else
      onChange(e)
  }

  render() {


    const { input, data, textField, filter, searchTerm, defaultOpen = true,
      itemComponent, containerClass, valueField, placeholder, label,
      meta: { touched, error, warning },
      onSearch } = this.props  //whatever props you send to the component from redux-form Field

    var value = ""
    if (input.value == "") {
      if (this.refs.dropdown) {
        this.refs.dropdown["_values"] = {}
      }
    }
    else {
      value = data.find((item: any) => item[valueField] == input.value)
    }


    if (data) {
      return (
        <div className={classnames({ [containerClass]: true,'row':true, 'has-error': touched && error })}>

          {(label && <label className={'control-label col-3'}>{label}</label>)}
          {!itemComponent && <div className="col-9" > <DropdownList   ref="dropdown" value={value} defaultValue={value} data={data} textField={textField} valueField={valueField} filter='contains' placeholder={placeholder} onChange={this.onChange} /></div>}
          {itemComponent && <div className="col-9" ><DropdownList filter={filter} onSearch={onSearch} ref="dropdown" value={value} defaultValue={value} data={data} itemComponent={itemComponent} valueField={valueField} placeholder={placeholder} onChange={this.onChange} open={defaultOpen} searchTerm={searchTerm} /></div>}

          {((touched && error && <span className="m-form__help text-danger"> {error} </span>) || (warning && <span>{warning}</span>))}

        </div>

      )
    }
    else {
      return ("")
    }

  }
}
