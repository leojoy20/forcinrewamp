import * as  React from 'react';
import { CustomProps } from "./props";
import classNames from 'classnames';
import * as  DateTime from 'react-datetime'
import moment = require('moment');

export class RenderDatePicker extends React.Component<CustomProps>{

  isValid(currentDate) {
    let { minDate, maxDate } = this.props
    let isValid = true

    if (minDate) {
      isValid = currentDate >= minDate
    }
    if (maxDate) {
      isValid = currentDate <= maxDate
    }
    return isValid
  }
  onChange(e) {

    const { input: { onChange } } = this.props
    onChange(e.format("YYYY-MM-DD"))
  }
  render() {
    const { input, label, disabled = false, containerClass, meta: { touched, error, warning } } = this.props


    return <div className={classNames({ [containerClass]: true,'row':true, 'has-error': touched && error })}>

      <label className={'control-label col-3'}>{label}</label>

      <DateTime className="col-9" onChange={this.onChange.bind(this)} placeholder="dd"  closeOnSelect={true} isValidDate={this.isValid.bind(this)} defaultValue={input.value ? moment(input.value, 'YYYY-MM-DD') : ""}  selected={input.value ? moment(input.value, 'YYYY-MM-DD') : null} disabled={disabled} dateFormat="YYYY-MM-DD" timeFormat={false} />
      {/* <DatePicker {...input} dateForm="YYYY-MM-DD" selected={input.value ? moment(input.value, 'YYYY-MM-DD') : null} /> */}
      {touched && ((error && <span className="help-block  text-danger"> {error} </span>) || (warning && <span>{warning}</span>))}

    </div>
  }
}
