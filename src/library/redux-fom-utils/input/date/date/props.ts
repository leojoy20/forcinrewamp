import { MetaProps } from '@forcin/library/redux-fom-utils/props';

export interface CustomProps{
  minDate:any
  maxDate:any
  input:any
  label:string
  disabled:boolean
  containerClass:string
  meta:MetaProps

}
