import * as React from "react";
import classnames from 'classnames';
import { FieldProps } from "./props";

export class RenderLabelField extends React.Component<FieldProps>{



  render() {
    const { input, label, containerClass, meta: { touched, error, warning } } = this.props
    let hasLabel = (typeof label != "undefined" || label != null)

    return (


      <div className={classnames({ [containerClass]: true, "row": true, 'has-error': touched && error })}>
        {(label && <label className="control-label  col-3" >{label}</label>)}
        <div className={classnames({ "col-9": hasLabel, "col-12": !hasLabel })}>
          {input.value}
        </div>
        {touched && ((error && <span className="m-form__help text-danger"> {error} </span>) || (warning && <span>{warning}</span>))}

      </div>
    )
  }
}



