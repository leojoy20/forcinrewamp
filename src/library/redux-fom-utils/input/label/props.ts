export interface FieldProps {
  input?: any,
  data?: any,
  textField?: any,
  placeholder?: any
  className?: any,
  defaultValue?: any,
  label?: any
  meta?: any,
  classname?: any,
  parentClass?:any,
  type?:any,
  name?:any,
  disabled?:Boolean,
  containerClass?:string
  passwordComponent:boolean

}

export interface StateProps{
  show:boolean,
  inputType:string
}
