import * as React from "react";
import classnames from 'classnames';
import { FieldProps,StateProps } from "./props";

export class RenderTextAreaField extends React.Component<FieldProps,StateProps>{

  state={
    show:false,
    inputType:"text"

  }

  componentDidMount(){
    this.setState({
      inputType:this.props.type
    })
  }
  changeVisibilityMode(){

    let type=!this.state.show?"text":"password"
    this.setState({
      show:!this.state.show,
      inputType:type
    })
  }


  render() {
    const { input, label, containerClass, disabled = false, passwordComponent = false, name, className, placeholder, meta: { touched, error, warning } } = this.props
    let hasLabel = (typeof label != "undefined" || label != null)

    return (


      <div className={classnames({ [containerClass]: true, "row": true, 'has-error': touched && error })}>
        {(label && <label className="control-label  col-3" >{label}</label>)}
        <div className={classnames({ "col-9": hasLabel, "col-12": !hasLabel })}>
          <textarea disabled={disabled}  {...input} style={{ width: '100%' }} name={name} className={classnames({ [className]: true, })} placeholder={placeholder} type={this.state.inputType} />
          {passwordComponent && <span style={{position:"absolute",top:10,right:3,cursor:"pointer"}} onClick={this.changeVisibilityMode.bind(this)}  className={classnames({"fa fa-fw fa-eye field-icon":!this.state.show,"fa fa-fw fa-eye-slash field-icon":this.state.show})} ></span>}

        </div>
        {touched && ((error && <span className="m-form__help text-danger"> {error} </span>) || (warning && <span>{warning}</span>))}

      </div>
    )
  }
}



