import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { baseUrl, AUTH_TOKEN } from "@forcin/constants/index";
import { LocalStorage } from "../storage/local-storage";

export class AxiosReq {
  private instance: AxiosInstance;
  private isAuthenticatedRoot: boolean;
  private createInstance() {
    this.instance = axios.create({
      baseURL: baseUrl
    });
    this.instance.defaults.timeout = 46000;
    let scope = this;
    this.instance.interceptors.request.use(function(config) {
      if (scope.isAuthenticatedRoot) {
        config.headers.common["Authorization"] =
          "Token " + LocalStorage.getItem(AUTH_TOKEN);
      }
      return config;
    });
    this.instance.interceptors.response.use(
      function(response) {
        // Do something with response data
        if (response.data) return response.data;
        else return response;
      },
      function(error) {
        //   if(error.response.status==401){
        //       localStorage.clear()
        //       location.reload()
        //   }
        // Do something with response error
        return Promise.reject(error);
      }
    );
  }

  constructor(isAuthenticatedRoot: boolean) {
    this.isAuthenticatedRoot = isAuthenticatedRoot;
    this.createInstance();
  }
  post(url: string, data: any, config?: AxiosRequestConfig): Promise<any> {
    return new Promise<any>((resolve: any, reject: any) => {
      this.instance
        .post(url, data, config)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  get(url: string, config: AxiosRequestConfig): Promise<any> {
    return new Promise<any>((resolve: any, reject: any) => {
      this.instance
        .get(url, config)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  put(url: string, data: any, config: AxiosRequestConfig): Promise<any> {
    return new Promise<any>((resolve: any, reject: any) => {
      this.instance
        .put(url, data, config)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
  delete(url: string, config: AxiosRequestConfig): Promise<any> {
    return new Promise<any>((resolve: any, reject: any) => {
      this.instance
        .delete(url, config)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
}
