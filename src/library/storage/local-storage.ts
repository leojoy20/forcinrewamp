
import {AES,MD5,enc}from 'crypto-js'
import { CRYPTO_KEY } from '@forcin/constants/index';
export  class LocalStorage {

    static getItem(key:string) {
        var ciphertext = MD5(key).toString();
        var encryptvalue = localStorage.getItem(ciphertext);
        if (encryptvalue == null)
            return null
        var bytes = AES.decrypt(encryptvalue, CRYPTO_KEY);
        var plaintext = bytes.toString(enc.Utf8);
        return plaintext;
    }

    static setItem(key:string, value:String) {
        var ciphertext = MD5(key).toString();
        let encryptedValue = AES.encrypt(value.toString(), CRYPTO_KEY);
        localStorage.setItem(ciphertext, encryptedValue.toString());
    }

    static removeItem(key:string) {
        var ciphertext = MD5(key).toString();
        localStorage.removeItem(ciphertext);
    }

}
