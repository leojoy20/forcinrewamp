import { Field, reduxForm, getFormSubmitErrors } from 'redux-form'
import { required } from "redux-form-validators";
import * as React from 'react';
import { CustomProps } from './props';
import { RenderTextField } from '@forcin/library/redux-fom-utils/input/text';
import { connect } from 'react-redux';



class SignIn extends React.Component<CustomProps>{

  render() {
    const { handleSubmit, submitting, pristine } = this.props
    // let image = require("@forcin/assets/images/forcin_logo.png")

    return <form onSubmit={handleSubmit} >
      <div className="m-portlet__body">
        <div className="row m-b-20">
          <div className="col-12">
            <div className="d-flex  align-items-center justify-content-center">
              <div className="logo-container">
                {/* <div style={{ backgroundImage: "url(" + image + ")" }} className="logo" ></div> */}

              </div>
            </div>
          </div>
        </div>

        <div className="form-group m-form__group row">

          <Field name="username" type="text"
            placeholder="Enter User Name" className="form-control"
            component={RenderTextField}
            containerClass="col-lg-12 m-b-10"
            iconClass="zmdi zmdi-account-circle"
            validate={[required({ message: "Please enter user name" })]}
          />

          <Field name="password" type="password"
            placeholder="Enter Password" className="form-control"
            component={RenderTextField}
            containerClass="col-lg-12 m-b-10"
            iconClass="zmdi zmdi-account-circle"
            validate={[required({ message: "Please enter password" })]}
          />
        </div>
        <div className="row text-center">
          <div className="col-12 text-center">

            <button type="submit" disabled={submitting || pristine} className="btn btn-secondary m-r-10">Sign In</button>

          </div>
        </div>
      </div>
    </form>


  }
}

let signInForm = reduxForm({
  form: 'create_sign_in',
  enableReinitialize: true,

})(SignIn)
const mapStateToProps = (state: any) => {
  return {
    submitErrors: getFormSubmitErrors('create_sign_in')(state),

  }
}
export default connect(mapStateToProps, null)(signInForm)


