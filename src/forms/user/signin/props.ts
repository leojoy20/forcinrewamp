export interface CustomProps {
  id?: Number
  handleSubmit?: any,
  reset?: any,
  submitting?: any,
  pristine?: any
}
