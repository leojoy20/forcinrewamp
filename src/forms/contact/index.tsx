import { Field, reduxForm, getFormSubmitErrors } from "redux-form";
import { required, email, numericality, length } from "redux-form-validators";
import * as React from "react";
import { CustomProps } from "./props";
import { RenderTextField } from "@forcin/library/redux-fom-utils/input/text";
import { connect } from "react-redux";
import { RenderTextAreaField } from '@forcin/library/redux-fom-utils/input/text-area';

class Contact extends React.Component<CustomProps> {
  render() {
    const { handleSubmit, submitting, pristine } = this.props;
    // let image = require("@forcin/assets/images/forcin_logo.png")

    return (
      <form onSubmit={handleSubmit}>
        <div className="m-portlet__body">
          <div className="row m-b-20">
            <div className="col-12">
              <div className="d-flex  align-items-center justify-content-center">
                <div className="logo-container">
                  {/* <div style={{ backgroundImage: "url(" + image + ")" }} className="logo" ></div> */}
                </div>
              </div>
            </div>
          </div>

          <div className="form-group m-form__group row">
            <Field
              name="name"
              type="text"
              placeholder="Name"
              className="form-control"
              component={RenderTextField}
              containerClass="col-lg-12 m-b-10"
              iconClass="zmdi zmdi-account-circle"
              validate={[required({ message: "Please enter Name" })]}
            />

            <Field
              name="companyName"
              type="text"
              placeholder="Company Name"
              className="form-control"
              component={RenderTextField}
              containerClass="col-lg-12 m-b-10"
              iconClass="zmdi zmdi-account-circle"
              validate={[required({ message: "Please enter Company Name" })]}
            />
            <Field
              name="email"
              type="text"
              placeholder="Email"
              className="form-control"
              component={RenderTextField}
              containerClass="col-lg-12 m-b-10"
              iconClass="zmdi zmdi-account-circle"
              validate={[
                required({ message: "Please enter Email" }),
                email({ message: "Please enter a valid email" })
              ]}
            />
            <Field
              name="phone"
              type="text"
              placeholder="Phone Number"
              className="form-control"
              component={RenderTextField}
              containerClass="col-lg-12 m-b-10"
              iconClass="zmdi zmdi-account-circle"
              validate={[
                required({ message: "Please enter Phone Number" }),
                numericality({
                  int: true,
                  message: "Please enter valid phone number"
                }),
                length({
                  min: 8,
                  max: 15,
                  message: "Please enter valid phone number"
                })
              ]}
            />
             <Field
              name="message"
              type="text"
              placeholder="Message"
              className="form-control"
              component={RenderTextAreaField}
              containerClass="col-lg-12 m-b-10"
              iconClass="zmdi zmdi-account-circle"
              validate={[
                required({ message: "Please enter Message" }),

              ]}
              ></Field>
          </div>
          <div className="row text-center">
            <div className="col-12 text-center">
              <button
                type="submit"
                disabled={submitting || pristine}
                className="btn btn-secondary m-r-10"
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

let signInForm = reduxForm({
  form: "contact_form",
  enableReinitialize: true
})(Contact);
const mapStateToProps = (state: any) => {
  return {
    submitErrors: getFormSubmitErrors("contact_form")(state)
  };
};
export default connect(
  mapStateToProps,
  null
)(signInForm);
