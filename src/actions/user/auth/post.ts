import { ApiUrls } from "@forcin/constants/urls/api";
import { dispatchToAuthStore } from "./util";
import { AxiosReq } from "@forcin/library/axios";
import { baseUrl } from "@forcin/constants/index";
import { Notifications } from "@forcin/component/notification";
import { NotificationType } from "@forcin/component/notification/notification-types";
import { UserActionTypes } from '@forcin/constants/action-type/user';

export const logIn = (userName: any, password: any) => {
  return dispatch => {
    new AxiosReq(false)
      .post(`${baseUrl}${ApiUrls.login}`, {
        username: userName,
        password: password
      })
      .then(response => {
        dispatch(dispatchToAuthStore(UserActionTypes.login, {}));
      })
      .catch(error => {
        ;
        Notifications.createNotification(NotificationType.error, {
          title: "Login Failed",
          message: "unable to login with provided credential"
        });
      });
  };
};
