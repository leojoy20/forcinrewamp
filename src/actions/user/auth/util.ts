import { AuthStateViewModel } from "@forcin/reducers/user/auth/state";
import { push } from 'connected-react-router';
import { AppReducer } from '@forcin/reducers/state';
import { UserActionTypes } from '@forcin/constants/action-type/user';

export const dispatchToAuthStore = (
  action: UserActionTypes,
  payload: AuthStateViewModel
) => {
  return {
    type: action,
    payload: payload
  };
};

export const gotoLogin = () => {
  return (dispatch, state:AppReducer) => {
    dispatch(
      push(`/signin`)
    )
  }
}

export const goToSignUp = () => {
  return (dispatch, state:AppReducer) => {
    dispatch(
      push(`/signup`)
    )
  }
}
