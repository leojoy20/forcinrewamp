export interface WindowSizes {
  ExtraLarge: boolean;
  Large: boolean;
  Medium: boolean;
  Small: boolean;
}

export const GetWindowSize = (windowSize: string): WindowSizes => {
  return {
    ExtraLarge: windowSize == "bp_infinity" ? true : false,
    Large: windowSize == "bp_1200" ? true : false,
    Medium: windowSize == "bp_900" ? true : false,
    Small: windowSize == "bp_580" ? true : false
  };
};
