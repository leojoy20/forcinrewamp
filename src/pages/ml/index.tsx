import * as React from "react";
import  {VerticleButton as ScrollUpButton} from "react-scroll-up-button";
import { ParallaxProvider } from 'react-scroll-parallax';
import Main from "@forcin/container/main/index";
import { Loader } from "@forcin/container/loader";

const PredictiveAnalytics = React.lazy(() => import("./predictive-analytics"));
const MachineLearning = React.lazy(() => import("./machine-learning"));
const ProductRecommendations = React.lazy(() => import("./product-recommendations"));

interface CustomProps {}
interface StateProps {
}

export class MachineLearningPage extends React.Component<CustomProps, StateProps> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    document
    .getElementById("root")
    .scrollIntoView({ block: "start", behavior: "smooth" });
  }

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main textStyle={{color:"#FFFF00"}} image={require("@forcin/assets/images/data_science.jpg")} title="Data Science" />
          <React.Suspense fallback={<Loader />}>
            <MachineLearning />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <PredictiveAnalytics />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <ProductRecommendations />
          </React.Suspense>
        <ScrollUpButton style={{zIndex:1000}}></ScrollUpButton>
        </ParallaxProvider>
      </React.Fragment>
    );
  }
}

export default MachineLearningPage;