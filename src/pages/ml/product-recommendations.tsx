import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {
}
class ProductRecommendations extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/ml/product-recommendations.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">Product Recommendations</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
              The package includes email services along with in-app recommendations. Product recommendations in e-commerce is done with the help of Recommendation Engines.
              </p>
              <p>
              Recommendation engines are basically data filtering tools that make use of algorithms and data to recommend the most relevant items to a particular user. Or in simple terms, they are nothing but an automated form of a “shop counter guy”. You ask him for a product. Not only he shows that product, but also the related ones which you could buy. They are well trained in cross-selling and up-selling.
              </p>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default ProductRecommendations;
