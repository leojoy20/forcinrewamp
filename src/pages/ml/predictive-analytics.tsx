import * as React from "react";
import ReactWOW from "react-wow";
import { Container, Row, Col,Image } from 'react-bootstrap';

export interface CustomProps {
}
class PredictiveAnalytics extends React.Component<CustomProps> {
  componentDidMount() {
  }
  render() {
    return (
      <Container fluid
      >

      <Row  className="m-t-40 m-b-30">
        <Col lg={5}>
          <ReactWOW delay="0.5s" animation="fadeIn">
            <h1 className=" m-t-20">Predictive Analytics</h1>
          </ReactWOW>
          <ReactWOW delay="0.6s" animation="fadeIn">
            <p>
            Predictive analytics extracts information from data sets to discover complex relationships, recognize unknown patterns, forecasting actual trends, find associations, etc. This allows you to anticipate the future and make the right decisions.
            </p>
            <p>
            Predictive analytics software applications use variables that can be measured and analyzed to predict the likely behavior of individuals, machinery or other entities. For example, an insurance company is likely to take into account potential driving safety variables, such as age, gender, location, type of vehicle and driving record, when pricing and issuing auto insurance policies.
            </p>
          </ReactWOW>
        </Col>
        <Col lg={7}>
          <Image className="img-fluid" src={require("@forcin/assets/images/ml/predictive-analytics.jpg")}></Image>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default PredictiveAnalytics;
