import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {
}
class MachineLearning extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/ml/ml.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">Machine Learning</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Machine learning is an application of artificial intelligence (AI) that provides systems the ability to automatically learn and improve from experience without being explicitly programmed. Machine learning focuses on the development of computer programs that can access data and use it learn for themselves.The process of learning begins with observations or data, such as examples, direct experience, or instruction, in order to look for patterns in data and make better decisions in the future based on the examples that we provide. The primary aim is to allow the computers learn automatically without human intervention or assistance and adjust actions accordingly.
              </p>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default MachineLearning;
