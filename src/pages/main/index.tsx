import * as React from "react";
import { ParallaxProvider } from "react-scroll-parallax";
import { Loader } from "@forcin/container/loader";
import Main from "@forcin/container/main/index";
// import { Fragment, Suspense } from "react";

const WhatWeDo = React.lazy(() => import("@forcin/container/what-we-do/index"));
const ContactUs = React.lazy(() =>
  import("@forcin/container/contact-us/index")
);
const HowAreWeWorking = React.lazy(() =>
  import("@forcin/container/how-are-we-working/index")
);
const OurMission = React.lazy(() =>
  import("@forcin/container/our-mission/index")
);
const AboutUs = React.lazy(() => import("@forcin/container/about-us/index"));
const Map = React.lazy(() => import("@forcin/container/map/index"));
import { VerticleButton as ScrollUpButton } from "react-scroll-up-button";

// import { Loader } from "@forcin/container/loader/index";
// import { GetWindowSize, WindowSizes } from "@forcin/utils/utils";
// import "@forcin/assets/css/custom.css";
// import "@forcin/assets/css/animate.min.css";
// import "@forcin/assets/css/hover-min.css";
// declare var $;
// declare var WebPro;
// import "@forcin/assets/js/waypoints.min";

interface CustomProps {}
interface StateProps {
  // windowSize: WindowSizes;
}

export class MainPage extends React.Component<CustomProps, StateProps> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    try {
      // window["Muse"].assets.check($); /* body */
      debugger;
    } catch (ex) {
      console.log(ex);
    }
  }

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main
            description="A step ahead in technologies."
            isMainPage={true}
            image={require("@forcin/assets/images/Home-2.jpg")}
          />

          <div id="services-page">
            <React.Suspense fallback={<Loader />}>
              <WhatWeDo />
            </React.Suspense>
          </div>

          <React.Suspense fallback={<Loader />}>
            <HowAreWeWorking />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <OurMission />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <AboutUs />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <ContactUs />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <Map />
          </React.Suspense>
        </ParallaxProvider>
        <ScrollUpButton style={{ zIndex: 1000 }}></ScrollUpButton>
      </React.Fragment>
    );
  }
}
