import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class Joomla extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/joomla.png")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">JOOMLA</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                JOOMLA is the widely used Content Management System (or CMS) to
                build dynamic and robust sites for a variety of purposes. It is
                capable of carrying out tasks ranging from corporate websites
                and blogs to social networks and e-commerce. It is easy to
                learn, quick to setup and inexpensive. We helps in delivering
                your product on time with flexible price plans.
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p>Responsive Web Development in Joomla</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> Joomla Theme and Extension Development</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p> Joomla Integration and Maintenance</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".9s"
                    duration=".9s"
                    animation="fadeIn"
                  >
                    <p>Joomla Optimization and SEO Services</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Joomla;
