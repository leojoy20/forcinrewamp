import * as React from "react";
import { VerticleButton as ScrollUpButton } from "react-scroll-up-button";
import { ParallaxProvider } from "react-scroll-parallax";
import Main from "@forcin/container/main/index";
import { Loader } from "@forcin/container/loader";

const Drupal = React.lazy(() => import("./drupal"));
const WordPress = React.lazy(() => import("./wordpress"));
const Joomla = React.lazy(() => import("./joomla"));

interface CustomProps {}
interface StateProps {}

export class CMSPage extends React.Component<CustomProps, StateProps> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    document
      .getElementById("root")
      .scrollIntoView({ block: "start", behavior: "smooth" });
  }

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main textStyle={{color:"#FFFF00 "}} image={require("@forcin/assets/images/CMS_main.jpg")} isMainPage={false} title="Content Management System" />
          <React.Suspense fallback={<Loader />}>
            <WordPress />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <Drupal />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <Joomla />
          </React.Suspense>
          <ScrollUpButton style={{ zIndex: 1000 }}></ScrollUpButton>
        </ParallaxProvider>
      </React.Fragment>
    );
  }
}

export default CMSPage;
