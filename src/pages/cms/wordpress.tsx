import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class WordPress extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/wordpress.png")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">WORDPRESS</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Wordpress is probably the easiest and most powerful Content
                Management System (or CMS) in existence today. It is very secure
                and a built-in Blog is there. We offer SEO friendly sites and
                easly update and navigate through your sites with faster
                delivery.
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p> WordPress Installation & Setup</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> WordPress custom plugin development</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p> WordPress Theme Development</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".9s"
                    duration=".9s"
                    animation="fadeIn"
                  >
                    <p>WordPress Template Design</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay="1.1s"
                    duration="1.1s"
                    animation="fadeIn"
                  >
                    <p>WordPress Blog Development</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay="1.2s"
                    duration="1.2s"
                    animation="fadeIn"
                  >
                    <p>WordPress Data Migration</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay="1.4s"
                    duration="1.4s"
                    animation="fadeIn"
                  >
                    <p>WordPress Maintenance & Upgrades</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default WordPress;
