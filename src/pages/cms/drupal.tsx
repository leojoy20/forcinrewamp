import * as React from "react";
import ReactWOW from "react-wow";
import { Container, Row, Col, Image } from "react-bootstrap";

export interface CustomProps {}
class Drupal extends React.Component<CustomProps> {
  componentDidMount() {}
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30">
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className=" m-t-20">DRUPAL</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Drupal has great standard features, like easy content authoring,
                reliable performance, and excellent security.Drupal shares
                content in multiple languages across many devices
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p>Development and Migration of Drupal</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> Drupal Custom Theme Development</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p> Joomla Integration and Maintenance</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".9s"
                    duration=".9s"
                    animation="fadeIn"
                  >
                    <p>Site Optimization and SEO Services</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".9s"
                    duration=".9s"
                    animation="fadeIn"
                  >
                    <p>Drupal Maintenance Solution</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/drupal.png")}
            ></Image>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default Drupal;
