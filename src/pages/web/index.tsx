import * as React from "react";
import { VerticleButton as ScrollUpButton } from "react-scroll-up-button";
import { ParallaxProvider } from "react-scroll-parallax";
import Main from "@forcin/container/main/index";
import { Loader } from "@forcin/container/loader";

const WebDevelopment = React.lazy(() => import("./web-dev"));

interface CustomProps {}
interface StateProps {}

export class WebDevPage extends React.Component<CustomProps, StateProps> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    document
      .getElementById("root")
      .scrollIntoView({ block: "start", behavior: "smooth" });
  }

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main textStyle={{color:"#FFFF00"}} image={require("@forcin/assets/images/web_development_main.jpg")} title="Web Development" isMainPage={false} />

          <React.Suspense fallback={<Loader />}>
            <WebDevelopment />
          </React.Suspense>
          <ScrollUpButton style={{ zIndex: 1000 }}></ScrollUpButton>
        </ParallaxProvider>
      </React.Fragment>
    );
  }
}

export default WebDevPage;
