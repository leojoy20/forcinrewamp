import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class WebDevelopment extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/web_development_content.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">WEB DEVELOPMENT</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Web development involves building reliable applications that
                process information over the Internet.With various application
                development frameworks and agile methodologies, we develop
                high-performance websites and applications that leads business
                objectives.
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p> Custom Web Applications</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> Websites and Portals</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p>App Backends</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".9s"
                    duration=".9s"
                    animation="fadeIn"
                  >
                    <p>ERP Systems</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default WebDevelopment;
