import * as React from "react";
import ReactWOW from "react-wow";
import { Container, Row, Col, Image } from "react-bootstrap";

export interface CustomProps {}
class Drupal extends React.Component<CustomProps> {
  componentDidMount() {}
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30">
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className=" m-t-20">IOS</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                We bring you an expertised team for building on the IOS
                platform.Our IOS applications are highly scalable, robust and of
                immense utility that the companies can improve their
                productivity and efficiency.The functionalities include gaming,
                social media integration, push messaging, web services
                integration, database synchronization and a lot more.
              </p>
            </ReactWOW>
          </Col>
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/mac.jpg")}
            ></Image>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default Drupal;
