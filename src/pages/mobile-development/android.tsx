import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class Joomla extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/android.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">ANDROID</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Android is versatile and user-friendly for mobile and tablet
                application platforms. As a leading Android Application
                Development team, we ensure the user feel the apps which are
                more user-friendly and more reliable with a budget-friendly
                price
              </p>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Joomla;
