import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class WordPress extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/crossplatform.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">CROSS-PLATFORM</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                We offer cross-platform compatibility of mobile apps with latest
                tools.When you want your apps to run on multiple platforms it
                tends to decrease costs and increase the speed at which apps are
                developed.
              </p>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default WordPress;
