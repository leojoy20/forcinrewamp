import * as React from "react";
import { VerticleButton as ScrollUpButton } from "react-scroll-up-button";
import { ParallaxProvider } from "react-scroll-parallax";
import Main from "@forcin/container/main/index";
import { Loader } from "@forcin/container/loader";

const IOS = React.lazy(() => import("./ios"));
const Android = React.lazy(() => import("./cross-platform"));
const CrossPlatform = React.lazy(() => import("./android"));

interface CustomProps {}
interface StateProps {}

export class MobileDevPage extends React.Component<
  CustomProps,
  StateProps
> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main image={require("@forcin/assets/images/mobile_application_main.jpg")} title=" Mobile Applications" />
          <React.Suspense fallback={<Loader />}>
            <Android />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <IOS />
          </React.Suspense>
          <React.Suspense fallback={<Loader />}>
            <CrossPlatform />
          </React.Suspense>
          <ScrollUpButton style={{ zIndex: 1000 }}></ScrollUpButton>
        </ParallaxProvider>
      </React.Fragment>
    );
  }
}

export default MobileDevPage;
