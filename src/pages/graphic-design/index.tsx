import * as React from "react";
import { VerticleButton as ScrollUpButton } from "react-scroll-up-button";
import { ParallaxProvider } from "react-scroll-parallax";
import Main from "@forcin/container/main/index";
import { Loader } from "@forcin/container/loader";

const GraphicDesign = React.lazy(() => import("./graphic-dev"));

interface CustomProps {}
interface StateProps {}

export class GraphicDesignPage extends React.Component<
  CustomProps,
  StateProps
> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    document
      .getElementById("root")
      .scrollIntoView({ block: "start", behavior: "smooth" });
  }

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main textStyle={{color:"#FFFF00"}} image={require("@forcin/assets/images/graphic_design_main.jpg")} title="Graphic Design" isMainPage={false} />

          <React.Suspense fallback={<Loader />}>
            <GraphicDesign />
          </React.Suspense>
          <ScrollUpButton style={{ zIndex: 1000 }}></ScrollUpButton>
        </ParallaxProvider>
      </React.Fragment>
    );
  }
}

export default GraphicDesignPage;
