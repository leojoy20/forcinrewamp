import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class WebDevelopment extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/graphic_design_content.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">GRAPHIC DESIGN</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Graphic design is the craft of creating visual content to
                communicate messages. We at Forcin, you can make your creative
                ideas to an innovative design patterns.
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p> Brochure Design</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> Logo Design</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p>Advertisements</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default WebDevelopment;
