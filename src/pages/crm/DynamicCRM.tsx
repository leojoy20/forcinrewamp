import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class Joomla extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/Dynamic_CRM.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">Dynamic CRM</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Dynamics CRM helps you to find potential customers and track the
                existing ones, automate tasks, manage operations and sales, use
                cloud corporate business applications.
              </p>
              <p>
                We are here for sales, marketing, and service professionals
                improve processes and better manage the customer experience by
                allowing users to access all of their customer-facing activities
                through a single interface.
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p> PLATFORM MIGRATION</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> SUPPORT AND ENHANCEMENT SERVICES</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p> CUSTOMIZING SERVICE DEVELOPMENT</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".9s"
                    duration=".9s"
                    animation="fadeIn"
                  >
                    <p>DATA INTEGRATION</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay="1.1s"
                    duration="1.1s"
                    animation="fadeIn"
                  >
                    <p>PLATFORM UPGRADATION</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay="1.2s"
                    duration="1.2s"
                    animation="fadeIn"
                  >
                    <p>CLOUD DEPLOYMENT SERVICES</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Joomla;
