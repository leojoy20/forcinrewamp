import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {}
class WordPress extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30" id="about-us">
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">Salesforce</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Salesforce's services allow businesses to use cloud technology
                to better connect with customers, partners and potential
                customers.
              </p>
              <p>
                Our Salesforce service helps to automate and manage your
                marketing, sales, and customer service functions, and drive
                digital transformation and innovation. 
              </p>
              <h4> OUR SERVICE OFFERINGS ARE:</h4>
              <ul>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".3s"
                    duration=".3s"
                    animation="fadeIn"
                  >
                    <p>
                      DEFINE AND MAP THE BUSINESS OBJECTIVES INTO SALESFORCE
                      DEVELOPMENT
                    </p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".5s"
                    duration=".5s"
                    animation="fadeIn"
                  >
                    <p> PLANNING IMPLEMENTATION AND CUSTOMIZATION</p>
                  </ReactWOW>
                </li>
                <li>
                  <ReactWOW
                    overflow={true}
                    delay=".7s"
                    duration=".7s"
                    animation="fadeIn"
                  >
                    <p> SALESFORCE MIGARTION AND INTEGRATION</p>
                  </ReactWOW>
                </li>
              </ul>
            </ReactWOW>
          </Col>
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/Salesforce.jpg")}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default WordPress;
