import * as React from "react";
import { VerticleButton as ScrollUpButton } from "react-scroll-up-button";
import { ParallaxProvider } from "react-scroll-parallax";
import Main from "@forcin/container/main/index";
import { Loader } from "@forcin/container/loader";

const SalesForce = React.lazy(() => import("./sales-force"));
const DynamicCRM = React.lazy(() => import("./DynamicCRM"));

interface CustomProps {}
interface StateProps {}

export class CRMPage extends React.Component<CustomProps, StateProps> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    document
      .getElementById("root")
      .scrollIntoView({ block: "start", behavior: "smooth" });
  }

  render() {
    return (
      <React.Fragment>
        <ParallaxProvider>
          <Main image={require("@forcin/assets/images/CRM-main.jpg")} title="Customer Relationship Management" />
          <React.Suspense fallback={<Loader />}>
            <SalesForce />
          </React.Suspense>

          <React.Suspense fallback={<Loader />}>
            <DynamicCRM />
          </React.Suspense>
          <ScrollUpButton style={{ zIndex: 1000 }}></ScrollUpButton>
        </ParallaxProvider>
      </React.Fragment>
    );
  }
}

export default CRMPage;
