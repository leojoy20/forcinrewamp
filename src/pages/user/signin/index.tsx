import * as React from "react";

import LoginForm from "@forcin/forms/user/signin";

// import "./style.scss";
import { CustomProps } from "./props";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import { logIn } from "@forcin/actions/user/auth/post";
import { bindActionCreators } from "redux";
// import { gotoLogin } from '@forcin/actions/user/auth/util';
import { Link } from "react-router-dom";

class SignIn extends React.Component<CustomProps> {
  /**
   *
   */
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(values: any) {
    this.props.logIn(values.username, values.password);
  }

  navigateToSignUp() {
    this.props.dispatch(push(`/signup`));
  }

  render() {
    return (
      <div className="row">
        <div className="col-12">
          <div className="d-flex  align-items-center justify-content-center">
            <div style={{ marginTop: "10%" }}>
              <LoginForm onSubmit={this.onSubmit} />

              <div
                style={{ height: 50, width: "100%" }}
                className="btn btn-default m-t-10"
              >
                <p>
                  New to forcin?{" "}
                  <Link to="signup" style={{ color: "blue" }}>
                    Create an account
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapActionsToProps = dispatch => {
  return bindActionCreators(
    {
      dispatch: dispatch,
      logIn: logIn
    },
    dispatch
  );
};

export default connect(
  null,
  mapActionsToProps
)(SignIn);
