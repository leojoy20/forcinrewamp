import { logIn } from '@forcin/actions/user/auth/post';
import { gotoLogin } from '@forcin/actions/user/auth/util';


export interface CustomProps{

  dispatch:any
  logIn:typeof logIn
  gotoLogin:typeof gotoLogin
}
