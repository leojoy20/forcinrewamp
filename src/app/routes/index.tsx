import { Switch } from "react-router";
import * as React from "react";
// import MainLayout from "../layout/main";
import RootLayout from "../layout/root";
import { MainPage } from "@forcin/pages/main";
import { MachineLearningPage } from "@forcin/pages/ml";
import { CMSPage } from "@forcin/pages/cms";
import { CRMPage } from "@forcin/pages/crm";
import { MobileDevPage } from "@forcin/pages/mobile-development";
import { GraphicDesignPage } from "@forcin/pages/graphic-design";
import { WebDevPage } from "@forcin/pages/web";
import { Notifications } from "@forcin/component/notification";
export class Routes extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <RootLayout
            key="ml"
            path="/ml"
            Component={MachineLearningPage}
            isAuthRequired={false}
          />
          <RootLayout
            key="cms"
            path="/cms"
            Component={CMSPage}
            isAuthRequired={false}
          />
          <RootLayout
            key="crm"
            path="/crm"
            Component={CRMPage}
            isAuthRequired={false}
          />
          <RootLayout
            key="mobile"
            path="/mobile"
            Component={MobileDevPage}
            isAuthRequired={false}
          />
          <RootLayout
            key="graphic"
            path="/graphic-design"
            Component={GraphicDesignPage}
            isAuthRequired={false}
          />
          <RootLayout
            key="web"
            path="/web"
            Component={WebDevPage}
            isAuthRequired={false}
          />
          <RootLayout
            key=""
            path="/"
            Component={MainPage}
            isAuthRequired={false}
          />
          {/* <MainLayout key="" path="/signup" Component={}   isAuthRequired={false}></MainLayout>
      <RootLayout key="" path="/" Component={}   isAuthRequired={true}></RootLayout> */}
        </Switch>
        {Notifications.renderNotificationContainer()}
      </React.Fragment>
    );
  }
}
