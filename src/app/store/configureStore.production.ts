import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import createRootReducer  from '@forcin/reducers/index';
import {  routerMiddleware } from 'connected-react-router'

const history = createBrowserHistory();
const router = routerMiddleware(history);
const enhancer = applyMiddleware(thunk, router);

export = {
  history,
  configureStore(initialState:any) {
    return createStore(createRootReducer (history), initialState, enhancer);
  }
};
