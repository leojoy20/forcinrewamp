
import { History } from 'history';

let configureStore: {
  history:History,
  configureStore:any
};

if (process.env.build === 'production') {
  configureStore = require('./configureStore.production');
} else {
  configureStore = require('./configureStore.development');
}
// configureStore = require('./configureStore.production');

export = configureStore;
