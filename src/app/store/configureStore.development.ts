import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createHashHistory } from 'history';
// import { routerMiddleware, push } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import createRootReducer  from '@forcin/reducers/index';

// import   loadingMiddleware  from 'redux-loading-middleware';

import {  routerMiddleware } from 'connected-react-router'

// import * as counterActions from '../actions/counter';

declare const window: Window & {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?(a: any): void;
};

declare const module: NodeModule & {
  hot?: {
    accept(...args: any[]): any;
  }
};

const actionCreators = Object.assign({},

);

const logger = (<any>createLogger)({
  level: 'info',
  collapsed: true
});

const history = createHashHistory();
const router = routerMiddleware(history);

// If Redux DevTools Extension is installed use it, otherwise use Redux compose
/* eslint-disable no-underscore-dangle */
const composeEnhancers: typeof compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Options: http://zalmoxisus.github.io/redux-devtools-extension/API/Arguments.html
    actionCreators
  }) as any :
  compose;
/* eslint-enable no-underscore-dangle */
const enhancer = composeEnhancers(
  applyMiddleware(thunk, logger,router)
);


export = {
  history,
  configureStore(initialState: any) {
    const store = createStore(createRootReducer(history), initialState, enhancer);

    if (module.hot) {
      module.hot.accept("@forcin/reducers", () =>
        store.replaceReducer(require('@forcin/reducers')) // eslint-disable-line global-require
      );
    }

    return store;
  }
};
