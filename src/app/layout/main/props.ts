import { BaseStateProps, BaseCustomProps } from '../base/props';

export interface CustomProps extends BaseCustomProps {

}
export interface StateProps extends BaseStateProps {
}
