import { CustomProps, StateProps } from "./props";
import BaseLayout from "../base";
import { Route } from 'react-router';
import React from 'react';
class MainLayout extends BaseLayout<CustomProps, StateProps> {
  checkPermission() {
    throw new Error("Method not implemented.");
  }
  isAuthenticated() {
    throw new Error("Method not implemented.");
  }
  componentWillMount(){
    super.componentDidMount()

  }
  render() {
    const { Component } = this.props;

    return (
      <Route
        render={matchProps => {
          return (
            <div>
              <Component {...matchProps} />
            </div>
          );
        }}
      />
    );
  }
  state = {};
  componentDidMount() {}

}

export default MainLayout;
