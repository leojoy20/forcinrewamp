import * as React from "react";
import { BaseCustomProps, BaseStateProps } from "./props";


abstract class BaseLayout<
  T extends BaseCustomProps,
  S extends BaseStateProps
> extends React.Component<T, S> {
  componentDidMount() {

    this.checkPermission();
    this.isAuthenticated();
  }
  abstract checkPermission();
  abstract isAuthenticated();
  abstract render(): React.ReactNode;

}

export default BaseLayout;
