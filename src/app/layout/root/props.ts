import { BaseStateProps, BaseCustomProps } from '../base/props';

export interface CustomProps extends BaseCustomProps {
  isAuthRequired: boolean;
}
export interface StateProps extends BaseStateProps {
}
