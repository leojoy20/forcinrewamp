import { CustomProps, StateProps } from "./props";
import BaseLayout from "../base";
import { Route } from "react-router";
import * as React from "react";
import "@forcin/assets/scss/common.scss";
import { NavBarComponent } from "@forcin/component/navbar";
import { Row ,Container, Col} from 'react-bootstrap';

class RootLayout extends BaseLayout<CustomProps, StateProps> {
  checkPermission() {
    return true;
  }
  isAuthenticated() {
    return true;
  }
  componentWillMount() {
    super.componentDidMount();
  }
  render() {
    const { Component } = this.props;

    return (
      <Route
        render={matchProps => {
          return (
            <React.Fragment>
              <NavBarComponent isInpage={true} />
              <Container fluid>
                <Row>
                  <Col lg={12} className="no-padding">
                    <Component {...matchProps} />
                  </Col>
                </Row>
              </Container>
            </React.Fragment>
          );
        }}
      />
    );
  }
  state = {};
  componentDidMount() {}
}

export default RootLayout;
