import { combineReducers, Reducer } from "redux";
import { reducer as formReducer } from 'redux-form'
import { UserReducer } from "./user";
import { routerReducer as routing } from 'react-router-redux';
import { AppReducer } from './state';
import { connectRouter } from 'connected-react-router';



export default (history) => combineReducers<AppReducer>({
  router: connectRouter(history),
  form: formReducer as any,
  ...UserReducer,
  routing: routing as Reducer<any>
})
