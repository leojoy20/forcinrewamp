import { IActionWithPayload } from "@forcin/library/helpers/action-helper";
import { AuthStateViewModel, AuthState } from "./state";
import { UserActionTypes } from "@forcin/constants/action-type/user";

export const AuthReducer = (
  state: AuthStateViewModel = AuthState,
  action: IActionWithPayload<AuthStateViewModel>
) => {
  switch (action.type) {
    case UserActionTypes.login:
      return { ...state, token: action.payload.token };

    default:
      return state;
  }
};
