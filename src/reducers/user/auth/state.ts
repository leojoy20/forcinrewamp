export interface AuthStateViewModel {
  token?: string;
  user_type?: string;
}

export const AuthState: AuthStateViewModel = {
  token: "",
  user_type: ""
};
