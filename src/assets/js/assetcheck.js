// window.Muse.assets.check = function(c) {
//   if (!window.Muse.assets.checked) {
//       window.Muse.assets.checked = !0;
//       var b = {},
//           d = function(a, b) {
//               if (window.getComputedStyle) {
//                   var c = window.getComputedStyle(a, null);
//                   return c && c.getPropertyValue(b) || c && c[b] || ""
//               }
//               if (document.documentElement.currentStyle) return (c = a.currentStyle) && c[b] || a.style && a.style[b] || "";
//               return ""
//           },
//           a = function(a) {
//               if (a.match(/^rgb/)) return a = a.replace(/\s+/g, "").match(/([\d\,]+)/gi)[0].split(","), (parseInt(a[0]) << 16) + (parseInt(a[1]) << 8) + parseInt(a[2]);
//               if (a.match(/^\#/)) return parseInt(a.substr(1),
//                   16);
//               return 0
//           },
//           f = function(f) {
//               for (var g = document.getElementsByTagName("link"), j = 0; j < g.length; j++)
//                   if ("text/css" == g[j].type) {
//                       var l = (g[j].href || "").match(/\/?css\/([\w\-]+\.css)\?crc=(\d+)/);
//                       if (!l || !l[1] || !l[2]) break;
//                       b[l[1]] = l[2]
//                   } g = document.createElement("div");
//               g.className = "version";
//               g.style.cssText = "display:none; width:1px; height:1px;";
//               document.getElementsByTagName("body")[0].appendChild(g);
//               for (j = 0; j < Muse.assets.required.length;) {
//                   var l = Muse.assets.required[j],
//                       k = l.match(/([\w\-\.]+)\.(\w+)$/),
//                       i = k && k[1] ?
//                       k[1] : null,
//                       k = k && k[2] ? k[2] : null;
//                   switch (k.toLowerCase()) {
//                       case "css":
//                           i = i.replace(/\W/gi, "_").replace(/^([^a-z])/gi, "_$1");
//                           g.className += " " + i;
//                           i = a(d(g, "color"));
//                           k = a(d(g, "backgroundColor"));
//                           i != 0 || k != 0 ? (Muse.assets.required.splice(j, 1), "undefined" != typeof b[l] && (i != b[l] >>> 24 || k != (b[l] & 16777215)) && Muse.assets.outOfDate.push(l)) : j++;
//                           g.className = "version";
//                           break;
//                       case "js":
//                           j++;
//                           break;
//                       default:
//                           throw Error("Unsupported file type: " + k);
//                   }
//               }
//               c ? c().jquery != "1.8.3" && Muse.assets.outOfDate.push("jquery-1.8.3.min.js") : Muse.assets.required.push("jquery-1.8.3.min.js");
//               g.parentNode.removeChild(g);
//               if (Muse.assets.outOfDate.length || Muse.assets.required.length) g = "Некоторые файлы на сервере могут отсутствовать или быть некорректными. Очистите кэш-память браузера и повторите попытку. Если проблему не удается устранить, свяжитесь с разработчиками сайта.", f && Muse.assets.outOfDate.length && (g += "\nOut of date: " + Muse.assets.outOfDate.join(",")), f && Muse.assets.required.length && (g += "\nMissing: " + Muse.assets.required.join(",")), suppressMissingFileError ? (g += "\nUse SuppressMissingFileError key in AppPrefs.xml to show missing file error pop up.", console.log(g)) : console.log(g)
//           };
//       location && location.search && location.search.match && location.search.match(/muse_debug/gi) ?
//           setTimeout(function() {
//               f(!0)
//           }, 5E3) : f()
//   }
// };