import * as React from "react";
import ReactWOW from "react-wow";
import { Row, Col, Image, Container } from "react-bootstrap";

export interface CustomProps {
  // windowSize: WindowSizes;
}
class OurMission extends React.Component<CustomProps> {
  render() {
    return (
      <Container fluid>
        <Row className="m-t-50 m-b-30" id="about-us">
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/mission.jpg")}
            />
          </Col>
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className="m-t-20">Our Mission</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                We are committed to deliver best industrial standards of our
                solutions.Our mission is to enhance the growth of our customers
                with innovative design, development and to deliver a high
                quality solutions at budget-friendly cost that create reliable
                competitive asset to customers around the world.
              </p>
            </ReactWOW>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default OurMission;
