import * as React from "react";

// import { WindowSizes } from "@forcin/utils/utils";
import { Container, Row, Col } from "react-bootstrap";
import ContactForm from "@forcin/forms/contact/index";
import "./contact-style.scss";
import  sweetAlert from "sweetalert2";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { reset } from "redux-form";
export interface CustomProps {
  // windowSize: WindowSizes;
  dispatch: any;
}
class ContactUs extends React.Component<CustomProps> {
  /**
   *
   */
  constructor(props) {
    super(props);
    this.onFormSucess = this.onFormSucess.bind(this);
  }
  componentDidMount() {
    // window["WOW"].sync()
  }
  onFormSucess(values) {
    sweetAlert.fire({
      title: "Message Sent",
      text:
        "Thank you for contacting us Our sales team will reach out you soon. ",
      type: "success"
    });
    this.props.dispatch(reset("contact_form"));
  }
  render() {
    return (
      <React.Fragment>
        <Container id="contat-page" fluid>
          <Row>
            <Col
              lg={6}
              style={{
                minHeight: 500,
                backgroundSize: "cover",
                backgroundImage: `url(${require("@forcin/assets/images/get-in-touch.jpeg")})`
              }}
            >
              <div
                className="m-t-50 m-l-10 text-color-white"
                style={{ padding: 10 }}
              >
                  <br />
                  <br />

                <h3>Forcin Software Solutions</h3>
                <hr className="sep-left" />
                <p className="m-t-50">
                  2nd Floor, B Block, Mather Square
                  <br />
                  Ernakulam North
                  <br />
                  Cochin
                  <br />
                  Kerala-682018
                  <br />
                  <a
                    className="link-line link-invert text-white"
                    href="mailto:info@forcin.in"
                  >
                     sales@forcin.in
                  </a>
                  <br />
                  <a
                    className="link-line link-invert text-white"
                    href="phone:914844042742"
                  >
                    +91484 40 42 742
                  </a>{" "}
                  <br />
                  <a
                    className="link-line link-invert text-white"
                    href="phone:919895429397"
                  >
                    +919895 42 93 97
                  </a>
                </p>
              </div>
            </Col>
            <Col className="contact-form" lg={6}>
              <div className="m-t-10 m-l-20">
                <h3>Contact Us </h3>
                <hr className="sep" />

                <div
                  role="form"
                  className="wpcf7"
                  id="wpcf7-f2276-o1"
                  lang="en-US"
                  dir="ltr"
                >
                  <div className="screen-reader-response"></div>
                  <ContactForm onSubmit={this.onFormSucess}></ContactForm>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ dispatch: dispatch }, dispatch);
};
export default connect(
  null,
  mapDispatchToProps
)(ContactUs);
