import * as React from "react";
// import { WindowSizes } from "@forcin/utils/utils";
import ReactWOW from "react-wow";
import { Container, Row, Col, Card } from "react-bootstrap";

export interface CustomProps {
  // windowSize: WindowSizes;
}
class HowAreWeWorking extends React.Component<CustomProps> {
  render() {
    return (
      <React.Fragment>
        <ReactWOW delay="0.5s" animation="fadeIn">
          <h1 className=" text-center m-b-20">How We are Working</h1>
        </ReactWOW>
        <Container fluid className={"m-t-30"}>
          <Row>
            <Col lg={3}>
              <Card>
                <Card.Img
                  variant="top"
                  src={require("@forcin/assets/images/business_meetings.jpeg")}
                />
                <Card.Body>
                  <ReactWOW
                    overflow={true}
                    delay="1s"
                    duration="1s"
                    animation="fadeIn"
                  >
                    <Card.Title>Business Meeting</Card.Title>
                    <Card.Text>
                      {/* CMS allows you to create, edit, manage and maintain
                        website pages on a single interface. We helps you
                        customize, update and manage your website data better
                        that helps in creating an efficient web presence,
                         easily accessible. */}
                    </Card.Text>
                  </ReactWOW>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={3}>
              <Card>
                <Card.Img
                  variant="top"
                  src={require("@forcin/assets/images/research.jpeg")}
                />
                <Card.Body>
                  <ReactWOW
                    overflow={true}
                    delay="1s"
                    duration="1s"
                    animation="fadeIn"
                  >
                    <Card.Title>Research</Card.Title>
                    <Card.Text>
                      {/* CMS allows you to create, edit, manage and maintain
                        website pages on a single interface. We helps you
                        customize, update and manage your website data better
                        that helps in creating an efficient web presence,
                         easily accessible. */}
                    </Card.Text>
                  </ReactWOW>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={3}>
              <Card>
                <Card.Img
                  variant="top"
                  src={require("@forcin/assets/images/web_development.jpg")}
                />
                <Card.Body>
                  <ReactWOW
                    overflow={true}
                    delay="1s"
                    duration="1s"
                    animation="fadeIn"
                  >
                    <Card.Title>Development</Card.Title>
                    <Card.Text>
                      {/* CMS allows you to create, edit, manage and maintain
                        website pages on a single interface. We helps you
                        customize, update and manage your website data better
                        that helps in creating an efficient web presence,
                         easily accessible. */}
                    </Card.Text>
                  </ReactWOW>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={3}>
              <Card>
                <Card.Img
                  variant="top"
                  src={require("@forcin/assets/images/implementation.jpeg")}
                />
                <Card.Body>
                  <ReactWOW
                    overflow={true}
                    delay="1s"
                    duration="1s"
                    animation="fadeIn"
                  >
                    <Card.Title>Implementation</Card.Title>
                    <Card.Text>
                      {/* CMS allows you to create, edit, manage and maintain
                        website pages on a single interface. We helps you
                        customize, update and manage your website data better
                        that helps in creating an efficient web presence,
                         easily accessible. */}
                    </Card.Text>
                  </ReactWOW>

                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

export default HowAreWeWorking;
