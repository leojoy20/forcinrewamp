import * as React from "react";
import { ParallaxBanner } from "react-scroll-parallax";
import { Form, FormControl, Button } from "react-bootstrap";
import { ClipFrame } from "@forcin/component/clip-frame";
import ReactWOW from "react-wow";

export interface CustomProps {
  // windowSize: WindowSizes;
}
class GetInTouch extends React.Component<CustomProps> {
  componentDidMount() {
    // window["WOW"].sync()
  }
  render() {
    return (
      <ParallaxBanner
        className="your-class"
        layers={[
          {
            image: require("@forcin/assets/images/get-in-touch.jpeg"),
            amount: 0.3,
            children: null
          },
          {
            image: require("@forcin/assets/images/get-in-touch.jpeg"),
            amount: 1,
            children: null
          },
          ,
          {
            image: require("@forcin/assets/images/get-in-touch.jpeg"),
            amount: 0.3,
            children: null
          }
        ]}
        style={{
          height: "700px"
        }}
      >
        <ClipFrame isBottom={false} />

        {/* <Image style={{maxHeight:700,width:"100%"}} src={require("@forcin/assets/images/get-in-touch.jpeg")} /> */}

        <div className="content-absolute">
          <ReactWOW delay="0.5s" animation="fadeIn">
            <h1 className="text-color-title">Get In Touch</h1>
          </ReactWOW>
          <ReactWOW delay="0.6s" animation="fadeIn">
            <Form className="call-back-form m-t-50" inline>
              <FormControl
                className=" m-b-5 custom-input"
                style={{ borderRadius: 20, marginLeft: "auto" }}
                placeholder="Name"
              />
              <FormControl
                className="m-l-5  m-b-5 custom-input"
                style={{ borderRadius: 20 }}
                placeholder="Mobile Number"
              />
              <Button
                className="m-l-5 m-b-5"
                style={{ borderRadius: 20, marginRight: "auto" }}
              >
                Call Me Back
              </Button>
            </Form>
          </ReactWOW>
        </div>
        <ClipFrame isBottom={true} />
      </ParallaxBanner>
    );
  }
}
export default GetInTouch;
