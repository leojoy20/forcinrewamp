import * as React from "react";
// import { WindowSizes } from "@forcin/utils/utils";
import ReactWOW from "react-wow";
import { Container, Row, Col, Image } from "react-bootstrap";

export interface CustomProps {
  // windowSize: WindowSizes;
}
class AboutUs extends React.Component<CustomProps> {
  componentDidMount() {
    // window["WOW"].sync()
  }
  render() {
    return (
      <Container fluid>
        <Row className="m-t-40 m-b-30">
          <Col lg={5}>
            <ReactWOW delay="0.5s" animation="fadeIn">
              <h1 className=" m-t-20">About Us</h1>
            </ReactWOW>
            <ReactWOW delay="0.6s" animation="fadeIn">
              <p>
                Company based in Cochin, Kerala, India that offers a wide range
                of services in the areas of Web Designing, Software Development
                and various Internet Marketing services. We offer our services
                worldwide on 24×7 bases and our team will be delivering the best
                services.The Prime motto of the firm is to provide extremely
                high-quality services quickly at a low cost. Therefore, if
                anyone needs a new website Design or Redesign, web development,
                e-commerce solutions, Logo & Graphic design, SEO Consulting. We
                can offer the best in quality and price. We are highly
                successful in our business because our objectives of achievement
                are dependent on the skills, knowledge and experience of the
                people carrying out the activity. We have an excellent
                development team capable of building anything from stunning
                brochures to complex online applications.
              </p>
            </ReactWOW>
          </Col>
          <Col lg={7}>
            <Image
              className="img-fluid"
              src={require("@forcin/assets/images/about-us.jpg")}
            ></Image>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default AboutUs;
