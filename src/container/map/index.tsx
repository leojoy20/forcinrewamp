import * as React from "react";
// import ReactWOW from "react-wow";
// import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import "./map-style.scss";
// import { ClipFrame } from "@forcin/component/clip-frame";
import GoogleMapReact from "google-map-react";
import ForcinMarker from "./map-marker";
// const AnyReactComponent = ({ text }) => <div>{text}</div>;

export interface CustomProps {
  // windowSize: WindowSizes;
}
class LeafLetMapForcin extends React.Component<CustomProps> {
  state = {
    lat: 9.992209,
    lng: 76.285823,
    zoom: 13
  };

  render() {
    const position = [this.state.lat, this.state.lng];

    return (
      <div style={{ width: "100%", height: 400 }}>
        {/* <ClipFrame isBottom={false} /> */}

        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyCOOLrkffgtW90C8-1WZ6QogyNwg5l_Npg" }}
          defaultCenter={position}
          defaultZoom={this.state.zoom}
        >
          <ForcinMarker
            lat={this.state.lat}
            lng={this.state.lng}
            text="Forcin Software Solutions"
          ></ForcinMarker>
          {/* <AnyReactComponent
            lat={59.955413}
            lng={30.337844}
            text="My Marker"
          /> */}
        </GoogleMapReact>
        {/* <Map
          center={position}
          zoom={this.state.zoom}
          doubleClickZoom={false}
          closePopupOnClick={false}
          dragging={false}
          zoomSnap={false}
          zoomDelta={false}
          trackResize={false}
          touchZoom={false}
          scrollWheelZoom={false}
        >
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </Map> */}
      </div>
    );
  }
}

export default LeafLetMapForcin;
