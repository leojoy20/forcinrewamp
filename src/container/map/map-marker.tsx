import * as React from "react";

interface CustomProps {
  text: any;
  lat: any;
  lng: any;
}

export default class ForcinMarker extends React.Component<CustomProps> {
  render() {
    return (
      <div style={{ width: 150 }}>
        <i className="fa fa-2x fa-map-marker"></i>
        <div className="pop-over-div">
          <p>{this.props.text}</p>
          <i className="pop-over-arrow fa  fas fa-sort-down">

          </i>
        </div>
      </div>
    );
  }
}
