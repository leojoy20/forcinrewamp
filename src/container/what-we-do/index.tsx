import * as React from "react";
import { Parallax } from "react-scroll-parallax";
import ReactWOW from "react-wow";
import { Card, Button, Row, Container, Col } from "react-bootstrap";
import { withRouter } from "react-router-dom";
// import { WindowSizes } from "@forcin/utils/utils";
export interface CustomProps {
  // windowSize: WindowSizes;
  history: any;
}

class WhatWeDo extends React.Component<CustomProps> {
  constructor(props) {
    super(props);
    this.onLinkClick = this.onLinkClick.bind(this);
  }

  onLinkClick(path) {
    document
      .getElementById("root")
      .scrollIntoView({ block: "start", behavior: "smooth" });
    setTimeout(() => {
      this.props.history.push(path);
    }, 700);
  }

  render() {
    return (
      <Parallax className="text-center m-10  ">
        <ReactWOW overflow={true} delay="0.5s" animation="fadeIn">
          <h1>What We Do</h1>
        </ReactWOW>
        <ReactWOW
          overflow={true}
          delay="0.5s"
          duration="0.15s"
          animation="fadeIn"
        >
          <Container fluid className="m-t-30">
            <Row>
              <Col className="m-t-10 m-b-20" lg={4}>
                <Card>
                  <Card.Img
                    variant="top"
                    src={require("@forcin/assets/images/web_development.jpg")}
                  />
                  <Card.Body>
                    <ReactWOW
                      overflow={true}
                      delay="1s"
                      duration="1s"
                      animation="fadeIn"
                    >
                      <Card.Title style={{ visibility: "hidden" }}>
                        Web Development
                      </Card.Title>
                      <Card.Text>
                        "Our emphasis on building complex web applications that
                        solve complex business problems. At Forcin, we develop
                        intuitive web applications that influences the growth of
                        clients' business, that too at a budget friendly price.
                      </Card.Text>
                    </ReactWOW>
                    <Button   onClick={() => this.onLinkClick("web")} variant="primary">Know More..</Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col className="m-t-10 m-b-20" lg={4}>
                <Card>
                  <Card.Img
                    variant="top"
                    src={require("@forcin/assets/images/graphic_design.jpg")}
                  />
                  <Card.Body>
                    <ReactWOW
                      overflow={true}
                      delay="1s"
                      duration="1s"
                      animation="fadeIn"
                    >
                      <Card.Title>Graphic Design</Card.Title>
                      <Card.Text>
                        Graphics are in terms of visual designs on any surface,
                        such as a wall, canvas, screen, paper etc. It has very
                        much importance in the Online World as it is used while
                        creating a logo, a website, or an advertisement.
                      </Card.Text>
                    </ReactWOW>
                    <Button   onClick={() => this.onLinkClick("graphic-design")} variant="primary">Know More..</Button>
                  </Card.Body>
                </Card>
              </Col>

              <Col className="m-t-10 m-b-20" lg={4}>
                <Card>
                  <Card.Img
                    variant="top"
                    src={require("@forcin/assets/images/mobile_app_development.jpeg")}
                  />
                  <Card.Body>
                    <ReactWOW
                      overflow={true}
                      delay="1s"
                      duration="1s"
                      animation="fadeIn"
                    >
                      <Card.Title>Mobile Application Development</Card.Title>
                      <Card.Text>
                        Forcin Software Solutions has a team of business
                        developers with advanced technology skills and extensive
                        experience in building mobile applications within the
                        platforms such as Android , iOS and Windows.
                      </Card.Text>
                    </ReactWOW>
                    <Button
                      onClick={() => this.onLinkClick("mobile")}
                      variant="primary"
                    >
                      Know More..
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col className="m-t-10 m-b-20" lg={4}>
                <Card>
                  <Card.Img
                    variant="top"
                    src={require("@forcin/assets/images/crm.jpg")}
                  />
                  <Card.Body>
                    <ReactWOW
                      overflow={true}
                      delay="1s"
                      duration="1s"
                      animation="fadeIn"
                    >
                      <Card.Title>CRM</Card.Title>
                      <Card.Text>
                        A CRM system allows businesses to manage business
                        relationships and the data and information associated
                        with them. We offer customer relationship management
                        solutions that are user-friendly with cost effective
                        price.
                      </Card.Text>
                    </ReactWOW>
                    <Button
                      onClick={() => this.onLinkClick("crm")}
                      className="btn btn-primary"
                    >
                      Know More..
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col className="m-t-10 m-b-20" lg={4}>
                <Card>
                  <Card.Img
                    variant="top"
                    src={require("@forcin/assets/images/ecommerce.jpg")}
                  />
                  <Card.Body>
                    <ReactWOW
                      overflow={true}
                      delay="1s"
                      duration="1s"
                      animation="fadeIn"
                    >
                      <Card.Title>Data Science</Card.Title>
                      <Card.Text>
                        We Provide cognitive computing, predictive modeling, and
                        real-time data analytics are used to extract actionable
                        intelligence. Let us demystify your dark data and
                        unravel insights that can keep you one step ahead of
                        competition
                      </Card.Text>
                    </ReactWOW>
                    <Button
                      onClick={() => this.onLinkClick("ml")}
                      className="btn btn-primary"
                    >
                      Know More..
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col className="m-t-10 m-b-20" lg={4}>
                <Card>
                  <Card.Img
                    variant="top"
                    src={require("@forcin/assets/images/web_development.jpg")}
                  />
                  <Card.Body>
                    <ReactWOW
                      overflow={true}
                      delay="1s"
                      duration="1s"
                      animation="fadeIn"
                    >
                      <Card.Title>CMS</Card.Title>
                      <Card.Text>
                        CMS allows you to create, edit, manage and maintain
                        website pages on a single interface. We helps you
                        customize, update and manage your website data better
                        that helps in creating an efficient web presence, easily
                        accessible.
                      </Card.Text>
                    </ReactWOW>
                    <Button
                      onClick={() => this.onLinkClick("cms")}
                      className="btn btn-primary"
                    >
                      Know More..
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </ReactWOW>
      </Parallax>
    );
  }
}

export default withRouter(WhatWeDo);
