import * as React from "react";
// declare var WebPro;
// import * as $ from "jquery";
// import classNames from "classnames";
// import { WindowSizes } from "utils/utils";
// import { Form, FormControl, Button } from "react-bootstrap";
export interface CustomProps {
  // windowSize: WindowSizes;
  image?: string;
  title?: string;
  description?: string;
  isMainPage?: boolean;
  textStyle?: any;
}
import { ParallaxBanner } from "react-scroll-parallax";
import { ClipFrame } from "@forcin/component/clip-frame";
import "./main.scss";
import ReactWOW from "react-wow";

class mainPage extends React.Component<CustomProps> {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}

  render() {
    return (
      <ParallaxBanner
        className="your-class"
        layers={[
          {
            image: this.props.image,
            amount: 0.3,
            children: null
          },
          {
            image: this.props.image,
            amount: 1,
            children: null
          },
          ,
          {
            image: this.props.image,
            amount: 0.3,
            children: null
          }
        ]}
        style={{
          height: "700px"
        }}
      >
        <div id="main-page" className="parallaxChildren">
          <div>

              {this.props.isMainPage && (
                <ReactWOW delay="0.4s" animation="fadeIn">
                  <h1 className="text-color-title text-h1-res">
                    {" "}
                    Forcin &nbsp; Software Solutions
                  </h1>
                  </ReactWOW>
              )}
              {!this.props.isMainPage && (
                <ReactWOW delay="0.4s" animation="fadeIn">

                <h1
                  style={{ ...(this.props.textStyle || {}) }}
                  className="text-color-title  text-h1-res"
                >
                  {this.props.title}
                </h1>
                  </ReactWOW>
              )}

            <ReactWOW delay="0.6s" animation="fadeIn">
              {/* <h3 className="text-center">A step ahead in technologies.</h3> */}
              <h3 className="text-center text-h3-res">
                {this.props.description}
              </h3>
            </ReactWOW>
          </div>
        </div>
        <ClipFrame isBottom={true} />
      </ParallaxBanner>
    );
  }
}

export default mainPage;
