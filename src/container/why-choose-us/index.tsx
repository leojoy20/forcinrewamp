import * as React from "react";
import { WindowSizes } from "@forcin/utils/utils";
export interface CustomProps {
  windowSize: WindowSizes;
}

class WhyChooseUs extends React.Component<CustomProps> {
  render() {
    return (
      <React.Fragment>
        {this.props.windowSize.ExtraLarge && (
          <React.Fragment>
            <div
              className="text big-title clearfix colelem shared_content"
              id="u142-5"
              data-content-guid="u142-5_content"
            >
              <h1 id="u142-3">
                W<span id="u142-2">hy Choose Us</span>
              </h1>
            </div>
            <div className="clearfix colelem" id="pu5160">
              <div
                className="rounded-corners transition clearfix grpelem shared_content"
                id="u5160"
                data-content-guid="u5160_content"
              >
                <div className="position_content" id="u5160_position_content">
                  <div
                    className="museBGSize clearfix colelem shared_content"
                    id="u5157"
                    data-content-guid="u5157_content"
                  >
                    <div className="museBGSize grpelem" id="u5158"></div>
                  </div>
                  <div
                    className="text title clearfix colelem shared_content"
                    id="u5159-4"
                    data-content-guid="u5159-4_content"
                  >
                    <h2 id="u5159-2">Development</h2>
                  </div>
                  <div
                    className="text main-text clearfix colelem shared_content"
                    id="u5155-4"
                    data-content-guid="u5155-4_content"
                  >
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Phasellus malesuada commodo convallis. Phasellus molestie
                      tincidunt justo, a porttitor arcu.
                    </p>
                  </div>
                </div>
              </div>
              <div
                className="rounded-corners transition clearfix grpelem shared_content"
                id="u5235"
                data-content-guid="u5235_content"
              >
                <div className="position_content" id="u5235_position_content">
                  <div
                    className="museBGSize clearfix colelem shared_content"
                    id="u5232"
                    data-content-guid="u5232_content"
                  >
                    <div className="museBGSize grpelem" id="u5233"></div>
                  </div>
                  <div
                    className="text title clearfix colelem shared_content"
                    id="u5234-4"
                    data-content-guid="u5234-4_content"
                  >
                    <h2 id="u5234-2">Security</h2>
                  </div>
                  <div
                    className="text main-text clearfix colelem shared_content"
                    id="u5230-4"
                    data-content-guid="u5230-4_content"
                  >
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Phasellus malesuada commodo convallis. Phasellus molestie
                      tincidunt justo, a porttitor arcu.
                    </p>
                  </div>
                </div>
              </div>
              <div
                className="rounded-corners transition clearfix grpelem shared_content"
                id="u5310"
                data-content-guid="u5310_content"
              >
                <div className="position_content" id="u5310_position_content">
                  <div
                    className="museBGSize clearfix colelem shared_content"
                    id="u5308"
                    data-content-guid="u5308_content"
                  >
                    <div className="museBGSize grpelem" id="u5307"></div>
                  </div>
                  <div
                    className="text title clearfix colelem shared_content"
                    id="u5309-4"
                    data-content-guid="u5309-4_content"
                  >
                    <h2 id="u5309-2">Design</h2>
                  </div>
                  <div
                    className="text main-text clearfix colelem shared_content"
                    id="u5305-4"
                    data-content-guid="u5305-4_content"
                  >
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Phasellus malesuada commodo convallis. Phasellus molestie
                      tincidunt justo, a porttitor arcu.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </React.Fragment>
        )}
        {this.props.windowSize.Large && (
          <React.Fragment>
            <span
              className="text big-title clearfix colelem placeholder"
              data-placeholder-for="u142-5_content"
            ></span>
            <div className="clearfix colelem temp_no_id" data-orig-id="pu5160">
              <div
                className="rounded-corners transition clearfix grpelem temp_no_id"
                data-orig-id="u5160"
              >
                <span
                  className="museBGSize clearfix colelem placeholder"
                  data-placeholder-for="u5157_content"
                ></span>
                <span
                  className="text title clearfix colelem placeholder"
                  data-placeholder-for="u5159-4_content"
                ></span>
                <span
                  className="text main-text clearfix colelem placeholder"
                  data-placeholder-for="u5155-4_content"
                ></span>
              </div>
              <span
                className="rounded-corners transition clearfix grpelem placeholder"
                data-placeholder-for="u5235_content"
              ></span>
              <span
                className="rounded-corners transition clearfix grpelem placeholder"
                data-placeholder-for="u5310_content"
              ></span>
            </div>
          </React.Fragment>
        )}
        {this.props.windowSize.Medium && (
          <React.Fragment>
            <span
              className="text big-title-900 clearfix colelem placeholder"
              data-placeholder-for="u135-5_content"
            ></span>
            <div className="clearfix colelem temp_no_id" data-orig-id="pu3778">
              <span
                className="process1 museBGSize grpelem placeholder"
                data-placeholder-for="u3778_content"
              ></span>
              <span
                className="arrow1 grpelem placeholder"
                data-placeholder-for="u3820_content"
              ></span>
              <span
                className="process2 museBGSize grpelem placeholder"
                data-placeholder-for="u3960_content"
              ></span>
            </div>
            <div className="clearfix colelem temp_no_id" data-orig-id="pu136-4">
              <span
                className="process1 title clearfix grpelem placeholder"
                data-placeholder-for="u136-4_content"
              ></span>
              <span
                className="process2 title clearfix grpelem placeholder"
                data-placeholder-for="u3956-4_content"
              ></span>
            </div>
            <div
              className="clearfix colelem temp_no_id"
              data-orig-id="pu3895-4"
            >
              <span
                className="process1 main-text clearfix grpelem placeholder"
                data-placeholder-for="u3895-4_content"
              ></span>
              <span
                className="process2 main-text clearfix grpelem placeholder"
                data-placeholder-for="u3957-4_content"
              ></span>
            </div>
            <span
              className="arrow2 colelem placeholder"
              data-placeholder-for="u4183_content"
            ></span>
            <span
              className="clearfix colelem placeholder"
              data-placeholder-for="pu4131_content"
            ></span>
            <div
              className="clearfix colelem temp_no_id"
              data-orig-id="pu4127-4"
            >
              <span
                className="process4 title clearfix grpelem placeholder"
                data-placeholder-for="u4127-4_content"
              ></span>
              <span
                className="process3 title clearfix grpelem placeholder"
                data-placeholder-for="u4013-4_content"
              ></span>
            </div>
            <div
              className="clearfix colelem temp_no_id"
              data-orig-id="pu4128-4"
            >
              <span
                className="process4 main-text clearfix grpelem placeholder"
                data-placeholder-for="u4128-4_content"
              ></span>
              <span
                className="process3 main-text clearfix grpelem placeholder"
                data-placeholder-for="u4014-4_content"
              ></span>
            </div>
          </React.Fragment>
        )}

        {this.props.windowSize.Small && (
          <React.Fragment>
            <span
              className="text big-title-580 clearfix colelem placeholder"
              data-placeholder-for="u142-5_content"
            ></span>
            <div
              className="rounded-corners transition clearfix colelem temp_no_id"
              data-orig-id="u5160"
            >
              <span
                className="museBGSize clearfix colelem placeholder"
                data-placeholder-for="u5157_content"
              ></span>
              <span
                className="text title-580 clearfix colelem placeholder"
                data-placeholder-for="u5159-4_content"
              ></span>
              <span
                className="text main-text clearfix colelem placeholder"
                data-placeholder-for="u5155-4_content"
              ></span>
            </div>
            <div
              className="rounded-corners transition clearfix colelem temp_no_id"
              data-orig-id="u5235"
            >
              <span
                className="museBGSize clearfix colelem placeholder"
                data-placeholder-for="u5232_content"
              ></span>
              <span
                className="text title-580 clearfix colelem placeholder"
                data-placeholder-for="u5234-4_content"
              ></span>
              <span
                className="text main-text clearfix colelem placeholder"
                data-placeholder-for="u5230-4_content"
              ></span>
            </div>
            <div className="clearfix colelem temp_no_id" data-orig-id="pu127-4">
              <span
                className="about title clearfix grpelem placeholder"
                data-placeholder-for="u127-4_content"
              ></span>
              <span
                className="browser_width grpelem placeholder"
                data-placeholder-for="u5392-bw_content"
              ></span>
              <div
                className="browser_width grpelem temp_no_id"
                data-orig-id="u5406-bw"
              >
                <div
                  className="rgba-background temp_no_id"
                  data-orig-id="u5406"
                ></div>
              </div>
              <span
                className="browser_width grpelem placeholder"
                data-placeholder-for="u5546-bw_content"
              ></span>
              <div
                className="PamphletWidget clearfix widget_invisible grpelem temp_no_id"
                data-islightbox="true"
                data-orig-id="pamphletu5588"
              >
                <div
                  className="ThumbGroup clearfix grpelem temp_no_id"
                  data-orig-id="u5589"
                >
                  <div
                    className="popup_anchor temp_no_id"
                    data-orig-id="u5592popup"
                  >
                    <div
                      className="Thumb popup_element clearfix temp_no_id"
                      data-orig-id="u5592"
                    >
                      <span
                        className="transition grpelem placeholder"
                        data-placeholder-for="u5436_content"
                      ></span>
                    </div>
                  </div>
                </div>
                <div
                  className="popup_anchor temp_no_id"
                  data-lightbox="true"
                  data-orig-id="u5599popup"
                >
                  <div
                    className="ContainerGroup rgba-background clearfix temp_no_id"
                    data-orig-id="u5599"
                  >
                    <div
                      className="Container invi clearfix grpelem temp_no_id"
                      data-orig-id="u5601"
                    >
                      <span
                        className="size_fixed grpelem placeholder"
                        data-placeholder-for="u5678_content"
                      ></span>
                    </div>
                  </div>
                </div>
                <div
                  className="popup_anchor temp_no_id"
                  data-orig-id="u5593popup"
                >
                  <div
                    className="PamphletCloseButton PamphletLightboxPart popup_element museBGSize temp_no_id"
                    data-orig-id="u5593"
                  ></div>
                </div>
              </div>
              <span
                className="browser_width grpelem placeholder"
                data-placeholder-for="u5456-bw_content"
              ></span>
              <div
                className="rounded-corners transition clearfix grpelem temp_no_id"
                data-orig-id="u5310"
              >
                <div
                  className="position_content temp_no_id"
                  data-orig-id="u5310_position_content"
                >
                  <span
                    className="museBGSize clearfix colelem placeholder"
                    data-placeholder-for="u5308_content"
                  ></span>
                  <span
                    className="text title-580 clearfix colelem placeholder"
                    data-placeholder-for="u5309-4_content"
                  ></span>
                  <span
                    className="text main-text clearfix colelem placeholder"
                    data-placeholder-for="u5305-4_content"
                  ></span>
                </div>
              </div>
              <span
                className="anchor_item grpelem placeholder"
                data-placeholder-for="about_content"
              ></span>
            </div>
            <span
              className="about2 main-text clearfix colelem placeholder"
              data-placeholder-for="u5715-4_content"
            ></span>
            <span
              className="imgright museBGSize rounded-corners colelem placeholder"
              data-placeholder-for="u5721_content"
            ></span>
            <span
              className="about title clearfix colelem placeholder"
              data-placeholder-for="u5751-4_content"
            ></span>
            <span
              className="about2 main-text clearfix colelem placeholder"
              data-placeholder-for="u5763-4_content"
            ></span>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default WhyChooseUs;
