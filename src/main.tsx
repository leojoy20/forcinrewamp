import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { configureStore, history } from "@forcin/app/store/configureStore";
import { Routes } from "@forcin/app/routes";
// import "@forcin/assets/scss/common.scss";
import "bootstrap";

// prepare store
const store = configureStore();
export { store };
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Routes />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
