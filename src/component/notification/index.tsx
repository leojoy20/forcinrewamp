import * as React from "react";
import { NotificationContainer, NotificationManager } from "react-notifications";
import { NotificationType } from './notification-types';

class Notifications extends React.Component {

    constructor(props) {
        super(props);
    }

    static createNotification(type:NotificationType, { message = "", title = undefined, timeOut = 10000, callback = null, priority = true, infiniteTime = false } = {}) {
        if (infiniteTime) {
            timeOut = 100000;
        }

        switch (type) {
            case "info":
                NotificationManager.info(message, title, timeOut, callback, priority);
                break;

            case "success":
                NotificationManager.success(message, title, timeOut, callback, priority);
                break;

            case "warning":
                NotificationManager.warning(message, title, timeOut, callback, priority);
                break;

            case NotificationType.error:
                NotificationManager.error(message, title, timeOut, callback, priority);
                break;
        }
    }

    static renderNotificationContainer() {
        return (
            <div>
                <NotificationContainer />
            </div>
        );
    }
}

export { Notifications};
