import * as React from "react";

export const ClipFrame = props => {
  let style = {};
  debugger;
  let isBottom = props.isBottom;
  if (isBottom) {
    style = { bottom: 0 };
  } else {
    // style = { top: 0 };
  }
  return (
    <div style={{ width: "100%", position: "absolute", ...style,zIndex:1000 }}>
      <img
        className="svg temp_no_img_src"
        id="u2344"
        width="100%"
        alt=""
        src={
          !isBottom
            ? require("@forcin/assets/images/element-21_poster_.png")
            : require("@forcin/assets/images/element-1_poster_.png??crc=4208392903")
        }
      />

    </div>
  );
};
