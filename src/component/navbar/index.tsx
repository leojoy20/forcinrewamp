import { Component } from "react";
import * as React from "react";
import { Navbar, Nav, Image } from "react-bootstrap";
import "./navbar-style.scss";
import * as classNames from "classnames";
import { withRouter } from "react-router";
interface CustomProps {
  isInpage: boolean;
  location: any;
  history: any;
}
interface StateProps {
  activePage: string;
}

class NavBarCustom extends Component<CustomProps, StateProps> {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.state = {
      activePage: "main-page"
    };
  }
  componentDidMount() {
    if (this.props.location.pathname != "/") {
      this.setState({
        activePage: "services-page"
      });
    } else {
      if (localStorage.getItem("link")) {
        debugger
        let link = localStorage.getItem("link");
       localStorage.clear()
        this.setState({
          activePage: link
        });
        document
          .getElementById(link)
          .scrollIntoView({ block: "start", behavior: "smooth" });
      }
    }
  }
  onClick(link, e: any) {
    if (this.props.location.pathname == "/") {
      debugger;
      e.preventDefault();
      this.setState({
        activePage: link
      });
      document
        .getElementById(link)
        .scrollIntoView({ block: "start", behavior: "smooth" });
    } else {
      debugger;
      localStorage.setItem("link", link);
      this.props.history.push("/");
    }
    // $(`#${link}`)
    //   .get(0)
    //   .scrollIntoView();
    // $("body")["scrollTo"](link);
  }

  render() {
    return (
      <Navbar
        className={classNames({ customNavBar: true })}
        bg="transparent"
        expand="lg"
      >
        <Navbar.Brand href="#home">
          <Image
            thumbnail
            width={50}
            className="plain-img"
            src={require("@forcin/assets/images/FocinLogo.png")}
          ></Image>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto m-r-10">
            <Nav.Link
              className={classNames({
                active: this.state.activePage == "main-page"
              })}
              onClick={e => this.onClick("main-page", e)}
            >
              Home
              <div className="underline"></div>
              <span></span>
            </Nav.Link>
            <Nav.Link
              className={classNames({
                active: this.state.activePage == "services-page"
              })}
              onClick={e => this.onClick("services-page", e)}
            >
              Services
              <div className="underline"></div>
            </Nav.Link>
            <Nav.Link
              className={classNames({
                active: this.state.activePage == "about-us"
              })}
              onClick={e => this.onClick("about-us", e)}
            >
              AboutUs
              <div className="underline"></div>
            </Nav.Link>
            <Nav.Link
              className={classNames({
                active: this.state.activePage == "contat-page"
              })}
              onClick={e => this.onClick("contat-page", e)}
            >
              Contact US
              <div className="underline"></div>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export const NavBarComponent = withRouter(NavBarCustom);
